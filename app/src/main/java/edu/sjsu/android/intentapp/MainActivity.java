package edu.sjsu.android.intentapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends Activity {
    final String myUriString = "http://www.amazon.com";
    final String phoneNumber = "tel:194912344444";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button browseButton = (Button) findViewById(R.id.browser);
        Button callButton = (Button) findViewById(R.id.call);
        browseButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent switchBrowser = new Intent (Intent.ACTION_VIEW, Uri.parse(myUriString));
                Intent chooser = Intent.createChooser(switchBrowser, "Please select an application");
                switchBrowser.putExtra("website", myUriString);
                startActivity(chooser);
                finish();
            }
        });
        callButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri number = Uri.parse(phoneNumber);
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                startActivity(callIntent);
                finish();
            }
        });

    }
}