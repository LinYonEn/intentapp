package edu.sjsu.android.mybrowser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private TextView text;
    private String web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.display);
        Intent url = getIntent();

        if(getIntent().getExtras() != null) {
            web = getIntent().getExtras().getString("website");
            text.setText(web);
        } else {
            Toast.makeText(this, "Nothing has been passed", Toast.LENGTH_LONG).show();
        }

    }
}